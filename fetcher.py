import os
import chardet
import sqlite3
import random
import requests
from PIL import Image
import math
from io import BytesIO
import signal
import logging
from requests.adapters import HTTPAdapter, Retry

# 定义一个信号处理函数，用于捕获Ctrl+C中断信号
def signal_handler(signal, frame):
    print("程序已中断")
    exit()

# 注册信号处理函数
signal.signal(signal.SIGINT, signal_handler)

# 创建日志记录器
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# 创建控制台处理程序，用于将日志输出到控制台
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

# 创建文件处理程序，用于将日志输出到文件
log_file = "log"  # 指定日志文件名
file_handler = logging.FileHandler(log_file)
file_handler.setLevel(logging.INFO)

# 创建格式化程序
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# 将格式化程序应用到处理程序
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

# 将处理程序添加到日志记录器
logger.addHandler(console_handler)
logger.addHandler(file_handler)

class Fetcher:

    def __init__(self, conditions, get_character_desc, get_religion_name, get_far_shot_desc, output_folder_name, proxies):
        self.get_far_shot_desc = get_far_shot_desc
        self.output_folder_name = output_folder_name
        self.get_character_desc = get_character_desc
        self.get_religion_name = get_religion_name
        self.proxies = proxies

        # 创建数据库连接
        self.conn = sqlite3.connect('info.db')

        # 创建游标对象
        self.cursor = self.conn.cursor()

        # 创建省份和国家map
        self.province_map = self.create_province_map(conditions)
        self.country_map = self.create_country_map()

        self.session = requests.Session()

        self.retries = Retry(total=5,
                        backoff_factor=1,
                        status_forcelist=[ 500, 502, 503, 504 ])

        self.session.mount('http://', HTTPAdapter(max_retries=self.retries))
        self.session.mount('https://', HTTPAdapter(max_retries=self.retries))

    def __del__(self):
        #self.conn.commit()
        self.conn.close()

    def create_province_map(self, conditions):
        self.cursor.execute('''
            SELECT provinces.*
            FROM provinces
            INNER JOIN countries ON provinces.owner = countries.country_tag
            WHERE provinces.owner IS NOT NULL
            AND provinces.culture IS NOT NULL
            AND provinces.religion IS NOT NULL
            AND countries.religion IS NOT NULL
            AND countries.technology_group IS NOT NULL
        ''' + 'AND (' + conditions + ')')
        results = self.cursor.fetchall()
        province_map = {}

        culture_dev = {}
        religion_dev = {}

        for province in results:
            province_id = province[0]
            culture = province[3]
            religion = self.get_religion_name(province[4])
            province_map[province_id] = province
            culture_dev[culture] = province[5] + (culture_dev[culture] if culture in culture_dev else 0)
            religion_dev[religion] = province[5] + (religion_dev[religion] if religion in religion_dev else 0)
        
        print(culture_dev)
        print(religion_dev)
            
        return province_map

    def create_country_map(self):
        self.cursor.execute('''
            SELECT *
            FROM countries
        ''')
        results = self.cursor.fetchall()
        country_map = {}
        for country in results:
            country_tag = country[0]
            country_map[country_tag] = country
        return country_map

    # 根据发展度随机选择一个省份
    def select_random_province(self):
        provinces = self.province_map.values()
        total_development = sum(province[5] for province in provinces)
        random_value = random.uniform(0, total_development)
        cumulative_development = 0
        for province in provinces:
            cumulative_development += province[5]
            if cumulative_development >= random_value:
                #self.logger.info(f"Chance of getting this province: {province[5]}/{total_development}")
                return self.country_map[province[2]], province

    def get_params_list(self, country, province):
        contents = [
                "after_handjob_cum_on_hands", "after_blowjob_cum_in_mouth", "after_titjob_cum_on_tits",
                "after_cumshot_cum_on_face", "after_creampie_cum_in_pussy",
                "cum_dripping_from_butt_crack",
                "cum_dripping_from_mouth", "cum_dripping_from_cleavage", "cum_dripping_from_nostril", "cum_dripping_from_pussy_while_squatting",
                "sexy_pose", "horny_pose",
                self.get_far_shot_desc(country),
                "sfw_sitting_alone", "sfw_lying(position)_alone", "nsfw_spreading_her_legs_alone",
                "during_sex_doggy_style_position_pov_from_behind", "during_sex_missionary_position_pov_from_above",
                "having_orgasm", "masturbating", "squirting"]
        params_list = []
        for content in contents:
            params = {}
            params["content"] = content
            params["character"] = self.get_character_desc(country)
            params["must_comply"] = "no_modern__no_duplicate__no_deformed"
            params["country"] = country[1]
            params["province"] = province[1]
            params["religion"] = self.get_religion_name(province[4])
            params["culture"] = province[3]
            params_list.append(params)
        return params_list

    def get_request_url_list(self, params_list, width, height, seeds):
        url_list = []
        for i, params in enumerate(params_list):
            query_str = ';'.join(f'{k}:{v}' for k, v in params.items())
            url = f"https://image.pollinations.ai/prompt/{query_str}?width={width}&height={height}&seed={seeds[i]}"
            url_list.append(url)
        return url_list

    def get_rand(self):
        return random.randint(0, (1 << 32) - 1)

    def download_image(self, url, filename, width, height):
        timeout = 120
        try:
            response = self.session.get(url, proxies=self.proxies, timeout=timeout)
            response.raise_for_status()
            content = response.content

            with Image.open(BytesIO(content)) as img:
                want = width / height
                got = img.size[0] / img.size[1]
                if not math.isclose(want, got, abs_tol=5e-2):
                    logger.warning(f"{filename} : 长宽比检查失败, want:{(width, height)}={want}, got:{img.size}={got}, 疑似远端返回了缓存的结果, 固舍弃.")
                    return False

            os.makedirs(os.path.dirname(filename), exist_ok=True)
            with open(filename, "wb") as f:
                f.write(content)
            logger.info(f"{filename} - {url}")
            return True
        except Exception as e:
            logger.error(f"{filename} - {e}")
            return False

    def get_images(self):
        country, province = self.select_random_province()
        params_list = self.get_params_list(country, province)
        width = 1000
        height = 500 + (hash(province) & 0x3f)
        seeds = [self.get_rand() for i in range(len(params_list))]
        url_list = self.get_request_url_list(params_list, width, height, seeds)

        logger.info(f"开始任务 - tag: {country}, prov: {province}")

        success_count = 0
        for i in range(len(params_list)):
            params = params_list[i]
            filename = f"{self.output_folder_name}/{params['content']}/{seeds[i]}_{params['country']}_{params['province']}_{params['culture']}_{params['religion']}.jpg"
            result = self.download_image(url_list[i], filename, width, height)
            if result:
                success_count += 1

        logger.info(f"成功任务数：{success_count} / 总任务数：{len(params_list)}")
        return success_count == len(params_list)




