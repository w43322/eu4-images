import os
import random
import requests
import signal
import logging
from requests.adapters import HTTPAdapter, Retry
import base64
from provinces import create_province_map
from PIL import Image, ImageDraw
import io
import random

version = "0.5.0"

# 定义一个信号处理函数，用于捕获Ctrl+C中断信号
def signal_handler(signal, frame):
    print("程序已中断")
    exit()

# 注册信号处理函数
signal.signal(signal.SIGINT, signal_handler)

# 创建日志记录器
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# 创建控制台处理程序，用于将日志输出到控制台
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

# 创建文件处理程序，用于将日志输出到文件
log_file = f"log_{version}"  # 指定日志文件名
file_handler = logging.FileHandler(log_file)
file_handler.setLevel(logging.INFO)

# 创建格式化程序
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# 将格式化程序应用到处理程序
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

# 将处理程序添加到日志记录器
logger.addHandler(console_handler)
logger.addHandler(file_handler)

class LocalFetcher:

    def __init__(self, output_folder_name, proxies):
        self.output_folder_name = output_folder_name
        self.proxies = proxies

        # 创建省份和国家map
        self.province_map = create_province_map()

        self.session = requests.Session()

        self.retries = Retry(total=5,
                        backoff_factor=1,
                        status_forcelist=[ 500, 502, 503, 504 ])

        self.session.mount('http://', HTTPAdapter(max_retries=self.retries))
        self.session.mount('https://', HTTPAdapter(max_retries=self.retries))

    def create_image_with_horizontal_line(self, seed):
        image = Image.new('L', (1200, 600), 'white')
        draw = ImageDraw.Draw(image)
        random.seed(seed)

        def dsl(a, b, n):
            if n == 0:
                return
            y = random.randint(a, b)
            draw.line((0, y, 1200, y), fill='black', width=15)
            dsl(y, b, n - 1)

        dsl(150, 450, 1)
        image_bytes = io.BytesIO()
        image.save(image_bytes, format='PNG')
        image_bytes.seek(0)
        return base64.b64encode(image_bytes.read()).decode()

    def get_pose_images(self, batch_size):
        images = []
        for i in range(batch_size):
            with open(os.path.join(os.getcwd(), "batch_images", f"{i+1}.png"), "rb") as f:
                images.append(base64.b64encode(f.read()).decode())
        return images

    def get_params_list(self, province):
        contents = {
            #"titjob": ["1boy, breasts, paizuri, penis, <lora:POVPaizuri:0.0>, breast squeeze, <lora:paizuri_1-000006:0.0>, titjob, <lora:titjob:0.0>, titfuck, a woman with a big breast is holding a cock, <lora:TitfuckXplicitContent:0.5>, pov, <lora:paizuri:0.0>, paizuri_keyword, 1girl, hetero, <lora:qqq-POV_paizuri-v2:0.0>, tittyfuck, boobjob, <lora:tittyfuck:0.0>, <lora:Titfuck_Realistic:0.3>, <lora:pov_titjob_v1:0.0>, <lora:TitFuck-v1:0.0>, pov tits fuck, <lora:all_tits_fuck.v1.0-000085:0.2>"],
            "cummed": ["outdoors, landmark, 1girl, standing, portrait, uncensored, ahegao, huge breasts, detailed eyes, detailed feminine face, looking at viewer, (cum on face:1.3), (cum on breasts:1.3), (cum on body:1.3)"],
            #"doggy style": [", a woman is getting fucked by a man, <lora:DoggystylePOV:0.5>, looking back, doggy_style, sex_from_behind, <lora:lora_doggystyle_32:0.05>, <lora:POVDoggy:0.1>, pov-doggy-graphos, penis in vagina, <lora:pov-doggy-graphos:0.025>, 1girl, 1boy, ass, hetero, penis, sex, solo focus, pussy, anus, sex from behind, pov, doggystyle, all fours, back, from behind, <lora:MS_Real_POVDoggyStyle_Lite:0.1>, hentai doggystyle, <lora:beihou:0.0125>"],
            #"missionary": ["1girl, 1boy, hetero, , missionarypose, penis, lying, missionary, vaginal, <lora:POVMissionary:0.3>, missionary vaginal, big breasts, <lora:PovMissionaryVaginal-v1:0.3>, pov, <lora:qqq-pov-missionary-v1:0.2>, missionary pov, <lora:missionary_pov.v2.0-000145:0.05>"],
        }

        def get_keywords(country, province, religion, culture, desc):
            return [f'({kw}:1.3)' for kw in [f'COUNTRY {country}', f'PROVINCE {province}',
                    f'RELIGION {religion}', f'CULTURE {culture}', desc, '15th century']]

        params_list = []
        for content, content_keywords in contents.items():
            params = {}
            params["country"] = province[1]
            params["province"] = province[0]
            params["religion"] = province[3]
            params["culture"] = province[4]
            params["content"] = content
            keywords = get_keywords(params["country"], params["province"], params["religion"], params["culture"], province[5])
            keywords += content_keywords
            params["prompt"] = ', '.join(keywords)
            params["np"] = "easynegative, ng_deepnegative_v1_75t, penis, hands, fat, milf, mom, 1boy, masculine face, horns, fingers, holding, modern"
            params_list.append(params)
        return params_list

    def get_payload_list(self, params_list, batch_size, seed, pose_images):
        payload_list = []
        for i, params in enumerate(params_list):
            payload = [{
                "prompt": params["prompt"],
                "negative_prompt": params["np"],
                "seed": seed + j,
                "sampler_name": "DPM++ 2M Karras",
                "batch_size": 1,
                "steps": 35,
                "cfg_scale": 10,
                "width": 1200,
                "height": 600,
                "alwayson_scripts": {
                    "ADetailer": {
                        "args": [
                            {
                                "ad_model": "face_yolov8n.pt",
                                "ad_mask_k_largest": 1,
                            }
                        ]
                    },
                    "controlnet": {
                        "args": [
                            {
                                "input_image": pose_images[j],
                                "model": "control_v11p_sd15_openpose [cab727d4]",
                                "weight": 0.75,
                                "resize_mode": 0,
                                "control_mode": 1,
                                "pixel_perfect": True,
                            },
                            {
                                "input_image": self.create_image_with_horizontal_line(seed+j),
                                "model": "control_v11p_sd15_mlsd [aca30ff0]",
                                "weight": 0.5,
                                "resize_mode": 0,
                                "control_mode": 1,
                                "guidance_end": 0.1,
                                "pixel_perfect": True,
                                "module": "mlsd",
                            },
                        ]
                    },
                }
            } for j in range(batch_size)]
            payload_list.append(payload)            
        return payload_list

    def get_rand(self):
        return random.randint(0, (1 << 32) - 1)

    def download_image(self, payload, filename, batch_size):
        timeout = 1200
        try:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            for i in range(batch_size):
                r = self.session.post("http://127.0.0.1:7860/sdapi/v1/txt2img", json=payload[i], proxies=self.proxies, timeout=timeout)
                r.raise_for_status()
                r = r.json()
                with open(f"{filename}.{i}.png", "wb") as f:
                    f.write(base64.b64decode(r['images'][0]))
            logger.info(f"{filename} - {payload[0]['prompt']}")
            return True
        except Exception as e:
            logger.error(f"{filename} - {e}")
            return False

    def select_country_and_province_by_provid(self, pid):
        if pid not in self.province_map:
            return True, None, None, None
        province = self.province_map[pid]
        tag = province[2]
        country = province[1]
        seed = (ord(tag[2]) << 4) | (ord(tag[1]) << 12) | (ord(tag[0]) << 20) | (pid << 28)
        return False, country, province, seed

    def gen_seq(self):
        res = []
        step = 100
        for i in range(0, step):
            for j in range(0, 10000, step):
                res.append(i + j)
        return res
    
    def get_idx_in_seq(self, seq, target):
        for i in range(len(seq)):
            if target == seq[i]:
                return i
        return None

    def get_images_by_provid(self, staring_provid, tot):
        batch_size = 8
        pose_images = self.get_pose_images(batch_size)
        seq = self.gen_seq()
        starting_idx = self.get_idx_in_seq(seq, staring_provid)
        curr = 0
        for idx in range(starting_idx, len(seq)):
            if curr == tot:
                break
            curr = curr + 1

            pid = seq[idx]
            skip, country, province, seed = self.select_country_and_province_by_provid(pid)
            if skip == True:
                logger.warning(f"跳过任务 - prov_id: {pid}")
                continue
            params_list = self.get_params_list(province)
            payload_list = self.get_payload_list(params_list, batch_size, seed, pose_images)

            logger.info(f"开始任务 - tag: {country}, prov: {pid}")

            success_count = 0
            for i in range(len(params_list)):
                params = params_list[i]
                filename = f"{self.output_folder_name}/{params['content']}/{province[5]}/{params['country']}_{params['province']}_{params['culture']}_{params['religion']}"
                result = self.download_image(payload_list[i], filename, batch_size)
                if result:
                    success_count += 1

            logger.info(f"成功任务数：{success_count} / 总任务数：{len(params_list)}")

def main(prefix):
    fetcher = LocalFetcher(prefix, None)
    fetcher.get_images_by_provid(0, 10000)

if __name__ == '__main__':
    main(version)
