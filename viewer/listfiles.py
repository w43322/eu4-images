import pathlib
import json
from http.server import HTTPServer, SimpleHTTPRequestHandler
from PIL import Image

WORKING_DIR = "C:/Github/eu4-images"
VERSION = "0.5.0"
IP = "127.0.0.1"
PORT = 36666

class Handler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, directory=WORKING_DIR)

def get_image_info(path):
    w, h = Image.open(path).size
    return {
        "s": f"http://{IP}:{PORT}/{path.as_uri().replace(f'file:///{WORKING_DIR}/', '')}",
        "w": w,
        "h": h,
    }

dir = pathlib.Path(WORKING_DIR + "/" + VERSION)
file_list = list(dir.rglob("*.png"))
print("got file list")

files = [get_image_info(path) for path in file_list]
with open(WORKING_DIR + "/viewer/files.json", "w") as f:
    json.dump(files, f)
print("dumped file list")

httpd = HTTPServer((IP, PORT), Handler)
httpd.serve_forever()
