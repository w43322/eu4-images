import PhotoSwipe from "https://unpkg.com/photoswipe/dist/photoswipe.esm.js";
import PhotoSwipeLightbox from "https://unpkg.com/photoswipe/dist/photoswipe-lightbox.esm.js";
import PhotoSwipeDynamicCaption from "https://unpkg.com/photoswipe-dynamic-caption-plugin/photoswipe-dynamic-caption-plugin.esm.js";

const FILES = await fetch("./files.json").then((response) => response.json());

function update(files, random) {
    if (random) {
        files.sort(() => Math.random() - 0.5)
    }
    const images = document.getElementById("images");
    images.textContent = "";
    for (const file of files) {
        const image = document.createElement("a");
        image.setAttribute("href", file["s"]);
        image.setAttribute("data-pswp-width", file["w"]);
        image.setAttribute("data-pswp-height", file["h"]);
        // image.textContent = file["s"];
        const img = document.createElement("img");
        img.setAttribute("src", file["s"]);
        img.setAttribute("width", file["w"] / 5);
        img.setAttribute("height", file["h"] / 5);
        const caption = document.createElement("div");
        caption.setAttribute("class", "pswp-caption-content");
        caption.textContent = file["s"];
        image.appendChild(img);
        image.appendChild(caption);
        images.appendChild(image);
    }
    const lightbox = new PhotoSwipeLightbox({
        gallery: '#images',
        children: 'a',
        // dynamic import is not supported in UMD version
        pswpModule: PhotoSwipe 
    });
    const captionPlugin = new PhotoSwipeDynamicCaption(lightbox, {
        // Plugins options, for example:
        type: 'below',
    });
    lightbox.init();
}

document.getElementById("filter").onclick = () => {
    const keywords = document.getElementById("keywords").value.split('\n');
    var files = FILES;
    for (const keyword of keywords) {
        files = files.filter((file) => {
            return file["s"].includes(keyword);
        });
        // console.log(keyword);
        // console.log(files);
    }
    update(files, document.getElementById("random").checked);
}
