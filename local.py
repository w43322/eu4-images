import os
import sqlite3
import random
import requests
from PIL import Image
import signal
import logging
from requests.adapters import HTTPAdapter, Retry
import io
import base64

version = "0.3.1.1"

# 定义一个信号处理函数，用于捕获Ctrl+C中断信号
def signal_handler(signal, frame):
    print("程序已中断")
    exit()

# 注册信号处理函数
signal.signal(signal.SIGINT, signal_handler)

# 创建日志记录器
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# 创建控制台处理程序，用于将日志输出到控制台
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)

# 创建文件处理程序，用于将日志输出到文件
log_file = f"log_{version}"  # 指定日志文件名
file_handler = logging.FileHandler(log_file)
file_handler.setLevel(logging.INFO)

# 创建格式化程序
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

# 将格式化程序应用到处理程序
console_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)

# 将处理程序添加到日志记录器
logger.addHandler(console_handler)
logger.addHandler(file_handler)

religion_name_map = {'coptic': 'Coptic', 'sunni': 'Sunni', 'shiite': 'Shia', 'hinduism': 'Hindu', 'animism': 'Animist', 'vajrayana': 'Vajrayana', 'buddhism': 'Theravada', 'ibadi': 'Ibadi', 'zoroastrian': 'Zoroastrian', 'shamanism': 'Fetishist', 'jewish': 'Jewish', 'shinto': 'Shinto', 'confucianism': 'Confucian', 'mahayana': 'Mahayana', 'tengri_pagan_reformed': 'Tengri', 'orthodox': 'Orthodox', 'dreamtime': 'Alcheringa', 'totemism': 'Totemist', 'nahuatl': 'Nahuatl', 'mesoamerican_religion': 'Mayan', 'inti': 'Inti', 'catholic': 'Catholic', 'hussite': 'Hussite'}

class LocalFetcher:

    def __init__(self, get_far_shot_desc, output_folder_name, proxies):
        self.get_far_shot_desc = get_far_shot_desc
        self.output_folder_name = output_folder_name
        self.proxies = proxies

        # 创建数据库连接
        self.conn = sqlite3.connect('info.db')

        # 创建游标对象
        self.cursor = self.conn.cursor()

        # 创建省份和国家map
        self.province_map = self.create_province_map()
        self.country_map = self.create_country_map()

        self.session = requests.Session()

        self.retries = Retry(total=5,
                        backoff_factor=1,
                        status_forcelist=[ 500, 502, 503, 504 ])

        self.session.mount('http://', HTTPAdapter(max_retries=self.retries))
        self.session.mount('https://', HTTPAdapter(max_retries=self.retries))

    def __del__(self):
        #self.conn.commit()
        self.conn.close()

    def create_province_map(self):
        self.cursor.execute('''
            SELECT *
            FROM provinces
            WHERE owner IS NOT NULL AND culture IS NOT NULL AND religion IS NOT NULL AND development IS NOT NULL
        ''')
        results = self.cursor.fetchall()
        province_map = {}

        culture_dev = {}
        religion_dev = {}

        for province in results:
            province_id = province[0]
            culture = province[3]
            religion = religion_name_map[province[4]]
            province_map[province_id] = province
            culture_dev[culture] = province[5] + (culture_dev[culture] if culture in culture_dev else 0)
            religion_dev[religion] = province[5] + (religion_dev[religion] if religion in religion_dev else 0)
        
        print(culture_dev)
        print(religion_dev)
            
        return province_map

    def create_country_map(self):
        self.cursor.execute('''
            SELECT *
            FROM countries
        ''')
        results = self.cursor.fetchall()
        country_map = {}
        for country in results:
            country_tag = country[0]
            country_map[country_tag] = country
        return country_map

    # 根据发展度随机选择一个省份
    def select_random_province(self):
        provinces = self.province_map.values()
        total_development = sum(province[5] for province in provinces)
        random_value = random.uniform(0, total_development)
        cumulative_development = 0
        for province in provinces:
            cumulative_development += province[5]
            if cumulative_development >= random_value:
                #self.logger.info(f"Chance of getting this province: {province[5]}/{total_development}")
                return self.country_map[province[2]], province

    def get_params_list(self, country, province, use_country=False):
        contents = {
            #"titjob": ["1boy, breasts, paizuri, penis, <lora:POVPaizuri:0.0>, breast squeeze, <lora:paizuri_1-000006:0.0>, titjob, <lora:titjob:0.0>, titfuck, a woman with a big breast is holding a cock, <lora:TitfuckXplicitContent:0.5>, pov, <lora:paizuri:0.0>, paizuri_keyword, 1girl, hetero, <lora:qqq-POV_paizuri-v2:0.0>, tittyfuck, boobjob, <lora:tittyfuck:0.0>, <lora:Titfuck_Realistic:0.3>, <lora:pov_titjob_v1:0.0>, <lora:TitFuck-v1:0.0>, pov tits fuck, <lora:all_tits_fuck.v1.0-000085:0.2>"],
            "cummed": ["outdoors, landmark, 1girl, uncensored, ahegao, detailed eyes, detailed feminine face, looking at viewer, (cum on face:1.3), (cum on breasts:1.3), (cum on body:1.3)"],
            #"doggy style": [", a woman is getting fucked by a man, <lora:DoggystylePOV:0.5>, looking back, doggy_style, sex_from_behind, <lora:lora_doggystyle_32:0.05>, <lora:POVDoggy:0.1>, pov-doggy-graphos, penis in vagina, <lora:pov-doggy-graphos:0.025>, 1girl, 1boy, ass, hetero, penis, sex, solo focus, pussy, anus, sex from behind, pov, doggystyle, all fours, back, from behind, <lora:MS_Real_POVDoggyStyle_Lite:0.1>, hentai doggystyle, <lora:beihou:0.0125>"],
            #"missionary": ["1girl, 1boy, hetero, , missionarypose, penis, lying, missionary, vaginal, <lora:POVMissionary:0.3>, missionary vaginal, big breasts, <lora:PovMissionaryVaginal-v1:0.3>, pov, <lora:qqq-pov-missionary-v1:0.2>, missionary pov, <lora:missionary_pov.v2.0-000145:0.05>"],
        }

        def get_keywords(country, province, religion, culture, tech_group):
            keywords = []

            if tech_group in ("aboriginal_tech", "andean", "mesoamerican", "north_american", "polynesian_tech", "south_american"):
                keywords.append("native chief")
            elif tech_group in ("east_african", "central_african", "sub_saharan"):
                keywords.append("ebony")
            elif tech_group in ("nomad_group"):
                keywords.append("nomad chief")

            if religion in ("sunni", "ibadi", "shiite"):
                keywords += ["hijab", "sultana"]
            elif religion in ("coptic", "catholic", "orthodox", "hussite", "jewish"):
                keywords += ["crown", "queen"]
            elif religion in ("mahayana", "hinduism", "vajrayana", "buddhism", "shinto", "confucianism"):
                keywords.append("empress")
            elif religion in ("tengri_pagan_reformed"):
                keywords.append("nomad chief")
            elif religion in ("animism", "shamanism", "dreamtime", "totemism", "nahuatl", "mesoamerican_religion", "inti"):
                keywords.append("tribal chief")

            return [f'({kw}:1.3)' for kw in [f'COUNTRY {country}', f'PROVINCE {province}',
                    f'RELIGION {religion_name_map[religion]}', f'CULTURE {culture}',
                    '15th century'] + list(dict.fromkeys(keywords))]

        params_list = []
        for content, content_keywords in contents.items():
            params = {}
            params["country"] = country[1]
            params["province"] = province[1]
            params["religion"] = country[2] if use_country else province[4]
            params["culture"] = country[4] if use_country else province[3]
            params["content"] = content
            keywords = get_keywords(params["country"], params["province"], params["religion"], params["culture"], country[3])
            keywords += content_keywords
            params["prompt"] = ', '.join(keywords)
            params_list.append(params)
        return params_list

    def get_payload_list(self, params_list, batch_size, seed):
        payload_list = []
        np = "EasyNegative, ng_deepnegative_v1_75t, penis, hands, fat, milf, mom,1boy, masculine face, hornspenis, hands, fat, milf, mom,1boy, masculine face, horns"
        for i, params in enumerate(params_list):
            payload0 = {
                "prompt": params["prompt"],
                "negative_prompt": np,
                "seed": seed,
                "batch_size": batch_size,
                "steps": 35,
                "cfg_scale": 10,
                "width": 400,
                "height": 600,
            }
            payload1 = {
                "prompt": params["prompt"],
                "negative_prompt": np,
                "seed": seed,
                "batch_size": batch_size,
                "steps": 35,
                "cfg_scale": 10,
                "width": 1200,
                "height": 600,
                "resize_mode": 2,
                "denoising_strength": 0.75,
                # useless "image_cfg_scale": 10,
            }
            payload_list.append([payload0, payload1])
        return payload_list

    def get_rand(self):
        return random.randint(0, (1 << 32) - 1)

    def download_image(self, payload, filename, batch_size):
        timeout = 600
        try:
            r0 = self.session.post("http://192.168.18.135:7860/sdapi/v1/txt2img", json=payload[0], proxies=self.proxies, timeout=timeout)
            r0.raise_for_status()
            r0 = r0.json()

            payload[1]['init_images'] = r0['images']
            r1 = self.session.post("http://192.168.18.135:7860/sdapi/v1/img2img", json=payload[1], proxies=self.proxies, timeout=timeout)
            r1.raise_for_status()
            r1 = r1.json()

            os.makedirs(os.path.dirname(filename), exist_ok=True)
            for i in range(batch_size):
                with open(f"{filename}.{i}.txt2img.png", "wb") as f:
                    f.write(base64.b64decode(r0['images'][i]))
                with open(f"{filename}.{i}.img2img.png", "wb") as f:
                    f.write(base64.b64decode(r1['images'][i]))
            logger.info(f"{filename} - {payload[0]['prompt']}")
            return True
        except Exception as e:
            logger.error(f"{filename} - {e}")
            return False

    def select_country_and_province_by_tag(self, tag):
        if tag not in self.country_map:
            return True, None, None, None
        country = self.country_map[tag]
        capital_id = country[5]
        if capital_id not in self.province_map:
            return True, None, None, None
        province = self.province_map[capital_id]
        if province[2] != tag:
            return True, None, None, None
        seed = (ord(tag[2]) << 3) | (ord(tag[1]) << 11) | (ord(tag[0]) << 19) | (capital_id << 27)
        return False, country, province, seed

    def select_country_and_province_by_provid(self, pid):
        if pid not in self.province_map:
            return True, None, None, None
        province = self.province_map[pid]
        tag = province[2]
        if tag not in self.country_map:
            return True, None, None, None
        country = self.country_map[tag]
        seed = (ord(tag[2]) << 3) | (ord(tag[1]) << 11) | (ord(tag[0]) << 19) | (pid << 27)
        return False, country, province, seed

    def get_images(self):
        batch_size = 8
        country, province = self.select_random_province()
        params_list = self.get_params_list(country, province)
        seed = self.get_rand()
        payload_list = self.get_payload_list(params_list, batch_size, seed)

        logger.info(f"开始任务 - tag: {country}, prov: {province}")

        success_count = 0
        for i in range(len(params_list)):
            params = params_list[i]
            filename = f"{self.output_folder_name}/{params['content']}/{params['country']}_{params['province']}_{params['culture']}_{religion_name_map[params['religion']]}-{seed}"
            result = self.download_image(payload_list[i], filename, batch_size)
            if result:
                success_count += 1

        logger.info(f"成功任务数：{success_count} / 总任务数：{len(params_list)}")
        return success_count == len(params_list)

    def get_images_by_tag(self, starting_tag):
        batch_size = 8
        for tag in self.country_map.keys():
            if tag < starting_tag:
                continue
            skip, country, province, seed = self.select_country_and_province_by_tag(tag)
            if skip == True:
                logger.warning(f"跳过任务 - tag: {tag}")
                continue
            params_list = self.get_params_list(country, province, True)
            payload_list = self.get_payload_list(params_list, batch_size, seed)

            logger.info(f"开始任务 - tag: {country}, prov: {province}")

            success_count = 0
            for i in range(len(params_list)):
                params = params_list[i]
                filename = f"{self.output_folder_name}/{country[3]}/{params['content']}/{params['country']}_{params['province']}_{params['culture']}_{params['religion']}"
                result = self.download_image(payload_list[i], filename, batch_size)
                if result:
                    success_count += 1

            logger.info(f"成功任务数：{success_count} / 总任务数：{len(params_list)}")

    def gen_seq(self):
        res = []
        step = 100
        for i in range(0, step):
            for j in range(0, 10000, step):
                res.append(i + j)
        return res
    
    def get_idx_in_seq(self, seq, target):
        for i in range(len(seq)):
            if target == seq[i]:
                return i
        return None

    def get_images_by_provid(self, staring_provid, tot):
        batch_size = 1
        seq = self.gen_seq()
        starting_idx = self.get_idx_in_seq(seq, staring_provid)
        curr = 0
        for idx in range(starting_idx, len(seq)):
            if curr == tot:
                break
            curr = curr + 1

            pid = seq[idx]
            skip, country, province, seed = self.select_country_and_province_by_provid(pid)
            if skip == True:
                logger.warning(f"跳过任务 - prov_id: {pid}")
                continue
            params_list = self.get_params_list(country, province, False)
            payload_list = self.get_payload_list(params_list, batch_size, seed)

            logger.info(f"开始任务 - tag: {country}, prov: {province}")

            success_count = 0
            for i in range(len(params_list)):
                params = params_list[i]
                filename = f"{self.output_folder_name}/{country[3]}/{params['content']}/{params['country']}_{params['province']}_{params['culture']}_{params['religion']}"
                result = self.download_image(payload_list[i], filename, batch_size)
                if result:
                    success_count += 1

            logger.info(f"成功任务数：{success_count} / 总任务数：{len(params_list)}")

def main(prefix):
    def get_far_shot_desc(country):
        if country[3] in ("aboriginal_tech", "andean", "mesoamerican", "north_american", "polynesian_tech", "south_american", "nomad_group", "central_african", "sub_saharan"):
            return "village"
        elif country[3] in ("western", "eastern", "ottoman", "chinese", "indian", "muslim"):
            return "palace"
        elif country[3] in ("east_african"):
            return "palace" if country[2] in ("sunni", "ibadi", "coptic", "jewish") else "village"
        return None

    fetcher = LocalFetcher(get_far_shot_desc, prefix, None)
    fetcher.get_images_by_provid(0, 10000)

if __name__ == '__main__':
    main(version)
