from fetcher import Fetcher
import sys

prefix = sys.argv[1]
fetchers = []

def get_character_desc_1(country):
    return "one_ahegao_20yo_girl_1500s_native_chief"

def get_religion_name_1(religion):
    names = {
        'dreamtime': 'Alcheringa',
        'animism': 'Animist',
        'totemism': 'Totemist',
        'nahuatl': 'Nahuatl',
        'mesoamerican_religion': 'Mayan',
        'inti': 'Inti',
    }
    return names[religion]

def get_far_shot_desc_1(country):
    return "sfw_far_shot_standing_in_front_of_her_village"

fetchers.append(Fetcher('countries.technology_group in ("aboriginal_tech", "andean", "mesoamerican", "north_american", "polynesian_tech", "south_american")',
                        get_character_desc_1,
                        get_religion_name_1,
                        get_far_shot_desc_1,
                        prefix + "/output_1",
                        None))



def get_character_desc_2(country):
    if country[3] == "ottoman":
        return "one_ahegao_20yo_girl_1500s_hijab_sultana"
    return "one_ahegao_20yo_girl_1500s_crown_queen"

def get_religion_name_2(religion):
    names = {
        'catholic': 'Catholic',
        'tengri_pagan_reformed': 'Tengri',
        'orthodox': 'Orthodox',
        'sunni': 'Sunni',
        'coptic': 'Coptic',
        'shiite': 'Shia',
        'hussite': 'Hussite',
    }
    return names[religion]

def get_far_shot_desc_2(country):
    return "sfw_far_shot_standing_in_front_of_her_palace"

fetchers.append(Fetcher('countries.technology_group in ("western", "eastern", "ottoman")',
                        get_character_desc_2,
                        get_religion_name_2,
                        get_far_shot_desc_2,
                        prefix + "/output_2",
                        None))

def get_character_desc_3(country):
    if country[3] == "nomad_group":
        return "one_ahegao_20yo_girl_1500s_nomad_chief"
    return "one_ahegao_20yo_girl_1500s_empress"

def get_religion_name_3(religion):
    names = {
        'shinto': 'Shinto',
        'confucianism': 'Confucian',
        'animism': 'Animist',
        'mahayana': 'Mahayana',
        'hinduism': 'Hindu',
        'tengri_pagan_reformed': 'Tengri',
        'sunni': 'Sunni',
        'buddhism': 'Theravada',
        'vajrayana': 'Vajrayana',
        'shiite': 'Shia',
        'orthodox': 'Orthodox',
    }
    return names[religion]

def get_far_shot_desc_3(country):
    if country[3] == "nomad_group":
        return "sfw_far_shot_standing_in_front_of_her_village"
    return "sfw_far_shot_standing_in_front_of_her_palace"

fetchers.append(Fetcher('countries.technology_group in ("chinese", "nomad_group")',
                        get_character_desc_3,
                        get_religion_name_3,
                        get_far_shot_desc_3,
                        prefix + "/output_3",
                        None))


def get_character_desc_4(country):
    if country[3] == "central_african" or country[3] == "sub_saharan":
        return "one_ahegao_20yo_girl_1500s_ebony_chief"
    elif country[2] == "sunni" or country[2] == "ibadi":
        return "one_ahegao_20yo_girl_1500s_ebony_hijab_sultana"
    elif country[2] == "coptic" or country[2] == "jewish":
        return "one_ahegao_20yo_girl_1500s_ebony_crown_queen"
    return "one_ahegao_20yo_girl_1500s_ebony_chief"

def get_religion_name_4(religion):
    names = {
        'sunni': 'Sunni',
        'shamanism': 'Fetishist',
        'ibadi': 'Ibadi',
        'coptic': 'Coptic',
        'shiite': 'Shia',
        'jewish': 'Jewish',
    }
    return names[religion]

def get_far_shot_desc_4(country):
    if country[3] == "central_african" or country[3] == "sub_saharan":
        return "sfw_far_shot_standing_in_front_of_her_village"
    elif country[2] == "sunni" or country[2] == "ibadi":
        return "sfw_far_shot_standing_in_front_of_her_palace"
    elif country[2] == "coptic" or country[2] == "jewish":
        return "sfw_far_shot_standing_in_front_of_her_palace"
    return "sfw_far_shot_standing_in_front_of_her_village"

fetchers.append(Fetcher('countries.technology_group in ("central_african", "east_african", "sub_saharan")',
                        get_character_desc_4,
                        get_religion_name_4,
                        get_far_shot_desc_4,
                        prefix + "/output_4",
                        None))


def get_character_desc_5(country):
    if country[3] == "muslim":
        return "one_ahegao_20yo_girl_1500s_hijab_sultana"
    return "one_ahegao_20yo_girl_1500s_indian_queen"

def get_religion_name_5(religion):
    names = {
        'coptic': 'Coptic',
        'sunni': 'Sunni',
        'shiite': 'Shia',
        'hinduism': 'Hindu',
        'animism': 'Animist',
        'vajrayana': 'Vajrayana',
        'buddhism': 'Theravada',
        'ibadi': 'Ibadi',
        'zoroastrian': 'Zoroastrian',
    }
    return names[religion]

def get_far_shot_desc_5(country):
    return "sfw_far_shot_standing_in_front_of_her_palace"

fetchers.append(Fetcher('countries.technology_group in ("indian", "muslim")',
                        get_character_desc_5,
                        get_religion_name_5,
                        get_far_shot_desc_5,
                        prefix + "/output_5",
                        None))


# 创建事件循环并运行主函数
while 1:
    for obj in fetchers:
        obj.get_images()
