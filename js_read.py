import requests
import concurrent.futures
import threading
import os


thread_local = threading.local()


def get_session():
    if not hasattr(thread_local, "session"):
        thread_local.session = requests.Session()
    return thread_local.session


def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches


def download_site(g_url):
    session = get_session()
    resp = session.get(g_url, proxies=proxies)
    resp.raise_for_status()
    a_str = resp.content.decode()
    all_occur = list(find_all(a_str, " href='"))
    path = "galleries/" + g_url.split('/')[-2] + "/"
    os.makedirs(os.path.dirname(path), exist_ok=True)
    for occur in all_occur:
        start = occur + 7
        end = a_str.find("'", start)
        url = a_str[start:end]
        resp = session.get(url, proxies=proxies)
        resp.raise_for_status()
        with open(path + url.split('/')[-1], "wb") as f:
            f.write(resp.content)

def download_all_sites(sites):
    with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
        executor.map(download_site, sites)


proxies = { 
              "http"  : "http://127.0.0.1:8889", 
              "https" : "http://127.0.0.1:8889",
            }

resp = requests.get("https://www.pornpics.com/search/srch.php?q=povd&lang=en&limit=99999&offset=0", proxies=proxies)

indexes = resp.json()

galleries = [index['g_url'] for index in indexes]

download_all_sites(galleries)


