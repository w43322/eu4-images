import re

def create_province_map():
    def read_html(file_path):
        with open(file_path, "r", encoding='utf-8') as f:
            return f.read()

    def get_tr(html_str):
        res = []
        tr_pattern = re.compile(r"<tr>[\S\r\n\t\f\v ]*?(<td>[\S\r\n\t\f\v ]*?)</tr>", re.MULTILINE)
        td_pattern = re.compile(r"<td>([\S\r\n\t\f\v ]*?)</td>", re.MULTILINE)
        for tr_match in re.finditer(tr_pattern, html_str):
            tr_str = tr_match.group(1)
            res.append([td_match.group(1) for td_match in re.finditer(td_pattern, tr_str)])
        return res

    def get_tag_map(trs):
        res = {}
        pattern = re.compile(r'<a.*?title="(.*?)">')
        for tds in trs:
            if len(tds) < 5:
                continue
            name = re.match(pattern, tds[1]).group(1)
            tag = tds[2]
            res[name] = tag
        return res

    def get_province_map(trs, tag_map):
        QUEEN = 'Queen'
        NOMAD = 'Nomadic Empress'
        SULTANA = 'Sultana'
        EMPRESS = 'Empress'
        NATIVE = 'Native Chief'
        EBONY = 'Ebony Chief'
        desc_map = {
            'Nordic': QUEEN,
            'East Slavic': QUEEN,
            'Baltic': QUEEN,
            'Germanic': QUEEN,
            'West Slavic': QUEEN,
            'French': QUEEN,
            'Latin': QUEEN,
            'South Slavic': QUEEN,
            'Carpathian': QUEEN,
            'Byzantine': QUEEN,
            'Iberian': QUEEN,
            'British': QUEEN,
            'Celtic': QUEEN,
            'Tatar': NOMAD,
            'Ugric': NOMAD,
            'Levantine': SULTANA,
            'Caucasian': QUEEN,
            'Maghrebi': SULTANA,
            'Iranian': SULTANA,
            'Altaic': NOMAD,
            'Western Aryan': EMPRESS,
            'Hindustani': EMPRESS,
            'Eastern Aryan': EMPRESS,
            'Dravidian': EMPRESS,
            'Central Indian': EMPRESS,
            'Tibetan': EMPRESS,
            'Burman': EMPRESS,
            'Tai': EMPRESS,
            'Mon-Khmer': EMPRESS,
            'Malay': EMPRESS,
            'Chinese': EMPRESS,
            'Evenki': NOMAD,
            'Korean': EMPRESS,
            'Caribbean': NATIVE, 
            'Tupi': NATIVE,
            'Je': NATIVE,
            'Sahelian': EBONY,
            'Araucanian': NATIVE,
            'East Bantu': EBONY,
            'Andean': NATIVE,
            'Kongo': EBONY,
            'Chibchan': NATIVE,
            'Mayan': NATIVE,
            'Otomanguean': NATIVE,
            'Central American': NATIVE,
            'Aridoamerican': NATIVE,
            'Sonoran': NATIVE,
            'Penutian': NATIVE,
            'Apachean': NATIVE,
            'Caddoan': NATIVE,
            'Muskogean': NATIVE,
            'Siouan': NATIVE,
            'Central Algonquian': NATIVE,
            'Iroquoian': NATIVE,
            'Eastern Algonquian': NATIVE,
            'Japanese': EMPRESS,
            'Kamchatkan': NATIVE,
            'Aboriginal': NATIVE,
            'Pacific': NATIVE,
            'Mande': EBONY,
            'West African': EBONY,
            'Southern African': EBONY,
            'Cushitic': EBONY,
            'Sudanese': EBONY,
            'Plains Algonquian': NATIVE,
            'Na-Déné': NATIVE,
            'Marañón': NATIVE,
            'Great Lakes': NATIVE
        }
        res = {}
        pattern = re.compile(r'<a.*?title="(.*?)">')
        for tds in trs:
            id = int(tds[0])
            name = tds[1].strip()
            m = re.match(pattern, tds[2])
            owner = m.group(1) if m else ""
            if id and owner:
                tag = tag_map[owner]
                m = re.match(pattern, tds[3])
                religion = m.group(1) if m else ""
                culture = tds[4].strip()
                m = tds[5].strip()
                culture_group = f"{m} {desc_map[m]}" if m else ""
                res[id] = (name, owner, tag, religion, culture, culture_group)
        return res


    c_html_str = read_html("countries.html")
    c_trs = get_tr(c_html_str)
    tag_map = get_tag_map(c_trs)

    p_html_str = read_html("provinces.html")
    p_trs = get_tr(p_html_str)
    province_map = get_province_map(p_trs, tag_map)

    return province_map
